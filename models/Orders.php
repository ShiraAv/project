<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $amount
 * @property integer $toppings
 * @property string $notes
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
       return [
            [['type', 'amount'], 'required'],
            [['type', 'amount', /*'toppings',*/ 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['notes'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
		 /*
		$rules = []; 
		$stringItems = [['notes'], 'string'];
		$integerItems  = ['type', 'amount', 'toppings', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'];		
		if (\Yii::$app->user->can('updateOrder')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems = [['name'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'amount' => 'Amount',
            'toppings' => 'Toppings',
            'notes' => 'Notes',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
	
	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	/*
	public function getToppingsItem()
    {
        return $this->hasMany(Toppings::className(), ['toppings' => 'id']);
    }*/
	
	public function getToppingsItem()
    {
        return $this->hasMany(Toppings::className(), ['id' => 'toppings']);
    }
	
	public function getTypeItem()
    {
        return $this->hasOne(Type::className(), ['id' => 'type']);
    }

}
