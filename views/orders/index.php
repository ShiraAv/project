<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'id',
				'label' => 'Order Number',
				'format' => 'raw',
			],	
            'name',
			[
				'attribute' => 'type',
				'label' => 'Type',
				'format' => 'raw',
				'value' => function($model){
					return $model->typeItem->name;
				},
			],	
            'amount',
			 [
				'attribute' => 'toppings',
				'label' => 'Toppings',
				'format' => 'raw',
				'value' => function($model){
					return $model->toppingsItem->name;
				},
			],

            'notes:ntext',
			 [
				'attribute' => 'status',
				'label' => 'Status',
				'format' => 'raw',
				'value' => function($model){
					return $model->statusItem->name;
				},
			],	
        
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	
		
</div>
