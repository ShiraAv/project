<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Orders;
use app\models\Status;
use app\models\Type;
use app\models\Toppings;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'type')->dropDownList(Type::getOrders()) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

	<?= $form->field($model, 'toppings')->checkBoxList(Toppings::getOrders()) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 4]) ?>

	<?= $form->field($model, 'status')->dropDownList(Status::getOrders()) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
