<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$employee = $auth->createRole('employee');
		$auth->add($employee);
		
		$shiftManager = $auth->createRole('shiftManager');
		$auth->add($shiftManager);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionEmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexOrders = $auth->createPermission('indexOrders');
		$indexOrders->description = 'All users can view orders';
		$auth->add($indexOrders);
		
		$viewOrder = $auth->createPermission('viewOrder');
		$viewOrder->description = 'View orders';
		$auth->add($viewOrder);
		
		$createOrder = $auth->createPermission('createOrder');
		$createOrder->description = 'Employee can create new leads';
		$auth->add($createOrder);
		
		$updateOrder = $auth->createPermission('updateOrder');
		$updateOrder->description = 'Employee can update orders';
		$auth->add($updateOrder);	

		$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'Every user can update his/her
									own profile ';
		$auth->add($updateOwnUser);
		 
		$updateOwnPassword  = $auth->createPermission('updateOwnPassword');
		$updateOwnPassword->description = 'VEvery user can update his/her
									own password';
		$auth->add($updateOwnPassword);		
	}


	public function actionSmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$deleteOrder = $auth->createPermission('deleteOrder');
		$deleteOrder->description = 'shiftManager can delete orders';
		$auth->add($deleteOrder);
				
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Admin can create new users';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);

		$updatePassword = $auth->createPermission('updatePassword');
		$updatePassword->description = 'Admin can update password for 
									all users';
		$auth->add($updatePassword);
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$employee = $auth->getRole('employee');

		$indexOrders = $auth->getPermission('indexOrders');
		$auth->addChild($employee, $indexOrders);

		$viewOrder = $auth->getPermission('viewOrder');
		$auth->addChild($employee, $viewOrder);
		
		$createOrder = $auth->getPermission('createOrder');
		$auth->addChild($employee, $createOrder);
		
		$updateOrder = $auth->getPermission('updateOrder');
		$auth->addChild($employee, $updateOrder);

		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->addChild($employee, $updateOwnUser);		
		
		$updateOwnPassword = $auth->getPermission('updateOwnPassword');
		$auth->addChild($employee, $updateOwnPassword);		
		
		$shiftManager = $auth->getRole('shiftManager');
		$auth->addChild($shiftManager, $employee);
		
		$deleteOrder = $auth->getPermission('deleteOrder');
		$auth->addChild($shiftManager, $deleteOrder);
		
		$admin = $auth->getRole('admin');
		$auth->addChild($admin, $shiftManager);		
		
		$createUser = $auth->getPermission('createUser');
		$auth->addChild($admin, $createUser);

		$updateUser = $auth->getPermission('updateUser');
		$auth->addChild($admin, $updateUser);
		
		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($admin, $deleteUser);		

		$updatePassword = $auth->getPermission('updatePassword');
		$auth->addChild($admin, $updatePassword);
	}
}