<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "toppings".
 *
 * @property integer $id
 * @property string $name
 */
class Toppings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'toppings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
		////Add to checkBox
	public static function getOrders()
	{
		$allOrders = self::find()->all();
		$allOrdersArray = ArrayHelper::
					map($allOrders, 'id', 'name');
		return $allOrdersArray;						
	}
	////Add to checkBox
	public function getOrdersItem()
	{
      return $this->hasOne(Orders::className(), ['id' => 'toppings']);
	}
	
}
